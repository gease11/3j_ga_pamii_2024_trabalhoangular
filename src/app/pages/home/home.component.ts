import { Component , OnInit} from '@angular/core';
import { consultarAPI } from 'src/app/dataset/dataset';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  MOVIES: any[] = []; 

  ngOnInit(): void {
    this.fetchAPI();
  }

  async fetchAPI() {
    try {
      const API_RESPONSE = await consultarAPI();
      this.MOVIES = API_RESPONSE.results; 
      console.log(this.MOVIES);
    } catch (error) {
      console.error('Error fetching API:', error);
    }
  }
}
