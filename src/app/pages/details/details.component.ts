import { Component,OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { consultarDetalhesAPI } from 'src/app/dataset/dataset';

@Component({
  selector: 'app-details',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  movieName: string = '' ;
  movieID: number = 0;
  MOVIE: any = {}; 


  constructor(
    readonly dadosRota: ActivatedRoute
  ){
    this.movieID = Number(this.dadosRota.snapshot.params['movieID']);
  }
  
  ngOnInit(): void {
    this.fetchAPI();
  }

  async fetchAPI() {
    try {
      const API_RESPONSE = await consultarDetalhesAPI(this.movieID);
      this.MOVIE = API_RESPONSE;
      console.log(this.MOVIE);
    } catch (error) {
      console.error('Error fetching API:', error);
    }
  }

  
}
