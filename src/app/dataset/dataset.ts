import axios from 'axios';

async function consultarAPI(){
    const options = {
        method: 'GET',
        url: 'https://api.themoviedb.org/3/discover/movie',
        params: {
          include_adult: 'false',
          include_video: 'false',
          language: 'pt-BR',
          page: '1',
          sort_by: 'popularity.desc',
          with_genres: '37'
        },
        headers: {
          accept: 'application/json',
          Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzNmM4YzExNzljNmUwMWY5MjZlMzhmODEyNTE5ZTdlMCIsInN1YiI6IjY1ZjhlY2VmNGI5YmFlMDE0NjdkOWE2NiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.rfmUgZrqm951IBEwLWP6IJX1KXnIUmC7vzaceg0T4r0'
        }
      };

    try {
        const response = await axios.request(options);
        return response.data;
    } catch (error) {
        throw error;
    }
}

export { consultarAPI }; 

async function consultarDetalhesAPI(movieID: number){
  const options = {
    method: 'GET',
    url: 'https://api.themoviedb.org/3/movie/' + movieID,
    params: {language: 'pt-BR'},
    headers: {
      accept: 'application/json',
      Authorization: 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzNmM4YzExNzljNmUwMWY5MjZlMzhmODEyNTE5ZTdlMCIsInN1YiI6IjY1ZjhlY2VmNGI5YmFlMDE0NjdkOWE2NiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.rfmUgZrqm951IBEwLWP6IJX1KXnIUmC7vzaceg0T4r0'
    }
  };

    try {
        const response = await axios.request(options);
        return response.data;
    } catch (error) {
        throw error;
    }
}

export { consultarDetalhesAPI }; 
